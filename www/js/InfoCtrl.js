AptekarApp.controller('InfoCtrl', ['$scope', '$stateParams', '$state', '$ionicLoading', 'drugService', '$ionicModal', function ($scope, $stateParams, $state, $ionicLoading, drugService, $ionicModal) {
    $scope.drug = {};
    $ionicLoading.show({
        template: '<ion-spinner icon="android"></ion-spinner>'
    });
    var drug_pomise;
    if ($stateParams.id)
        drug_pomise = drugService.getDrugInfoById($stateParams.id);
    else if ($stateParams.barcode)
        drug_pomise = drugService.getDrugInfoByBarcode($stateParams.barcode);
    drug_pomise.then(function (data) {
        $scope.drug = data;
        $ionicLoading.hide()
    });
    $scope.report = function () {
        $state.go('report', {barcode: this.barcode});
    };
    $ionicModal.fromTemplateUrl('info_modal.html', {
        scope: $scope,
        animation: 'slide-left-right'
    }).then(function (modal) {
        $scope.modal = modal;
    });
    $scope.$on('$destroy', function() {
        $scope.modal.remove();
    });
    $scope.moreinfo = function(text){
        $scope.text = $scope.drug[text];
        $scope.modal.show();
    };
    $scope.close_modal = function(){
        $scope.modal.hide();
    }
}]);