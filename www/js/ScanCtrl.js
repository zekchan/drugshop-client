AptekarApp.controller('ScanCtrl', ['$scope', '$cordovaBarcodeScanner', '$state', 'drugService', function ($scope, $cordovaBarcodeScanner, $state, drugService) {
    $scope.drugs = [];
    $scope.search = '';
    $scope.scanBarcode = function () { //starting barcode sca
        $cordovaBarcodeScanner.scan().then(function (imageData) {
            if (imageData.text) {
                $state.go('info', {barcode: imageData.text, id: 0});
            }
        });
    };
    $scope.filter = function(){
      drugService.getDrugsListByName(this.search).then(function(drugs){
              $scope.drugs = drugs;
          }
      )
    };
    $scope.choose = function(id){
        $state.go('info', {bar: 0, id: id});
    }
}]);