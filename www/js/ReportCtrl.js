AptekarApp.controller('ReportCtrl', ['$scope', '$stateParams', '$state', '$cordovaGeolocation', 'shopsService', function ($scope, $stateParams, $state, $cordovaGeolocation, shopsService) {
    $scope.barcode = $stateParams.barcode;
    $scope.loading = true;
    $scope.choosen_shop_id = 0;
    $scope.choosen_shop_name = "Выберите аптеку на карте";
    geo_promise = $cordovaGeolocation.getCurrentPosition({timeout: 10000, enableHighAccuracy: true});
    _this = $scope;
    function addMarker(data) {
        pos = new google.maps.LatLng(data.Lon, data.Lat);
        var marker = new google.maps.Marker({
            position: pos,
            map: $scope.map,
            title: data.Name,
            draggable: false,
            animation: google.maps.Animation.DROP,
            customInfo: {
                Id: data.Id,
                Name: data.Name
            }
        });
        google.maps.event.addListener(marker, 'click', function () {
            $scope.$apply(function () {
                $scope.choosen_shop_id = marker.customInfo.Id;
                $scope.choosen_shop_name = marker.customInfo.Name;
            })
        });
    }

    $scope.mapCreated = function (map) {
        $scope.map = map;
        geo_promise.then(function (pos) {
            shops_promise = shopsService.getShops(pos.coords.latitude, pos.coords.longitude);
            $scope.lat = pos.coords.latitude;
            $scope.long = pos.coords.longitude;
            var googleloc = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
            $scope.map.setCenter(googleloc);
            shops_promise.then(function () {
                $scope.shops = data;
                for (var i = 0; i < data.length; i++) {
                    addMarker(data[i]);
                }
            })
        }, function (error) {
            alert('Unable to get location: ' + error.message);
        });
    };
    $scope.sendreport = function () {
        alert("Спасибо за проявленную инициативу!");
    }
}]);